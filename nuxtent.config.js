/* eslint-disable import/no-unresolved */
/* eslint-disable import/no-extraneous-dependencies */
const markdownContainer = require('markdown-it-container')
const markdownItAttrs = require('markdown-it-attrs')
const implicitFigures = require('markdown-it-implicit-figures')

const implicit = [
  implicitFigures,
  {
    // <figure data-type="image">, default: false
    dataType: false,
    // <figcaption>alternative text</figcaption>, default: false
    figcaption: false,
    // <figure tabindex="1+n">..., default: false
    tabindex: true,
    // <a href="img.png"><img src="img.png"></a>, default: false
    link: false,
    copyAttrs: true,
  },
]

const {
  section,
  columns,
  column,
  notification,
} = require('./utils/markdownContainers')

module.exports = {
  content: {
    page: '_slug',
    permalink: '/:slug',
    isPost: false,
    generate: ['get', 'getAll'],
  },
  api(isStatic) {
    return {
      baseURL: 'http://localhost:3000',
      browserBaseURL: isStatic ? 'http://plantilla.dinamo.mx' : '',
    }
  },
  markdown: {
    plugins: {
      implicit,
      markdownItAttrs,
      section: [markdownContainer, 'section', section],
      columns: [markdownContainer, 'columns', columns],
      column: [markdownContainer, 'column', column],
      notification: [markdownContainer, 'notification', notification],
    },
  },
}
