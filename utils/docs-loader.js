/**
 * Loader para etiquetas <docs></docs>
 * @file: utils/docs-loader.js
 * @description Habilita la carga de la etiqueta <docs></docs>
 * para mantener la compatibilidad con vue-docs
 *
 * @param {Object} source - The source of the vue Component
 * @param {Function} map - The vue function
 * @returns {Void} - Nothing
 */
module.exports = function docsLoader(source, map) {
  this.callback(
    null,
    `export default function (Component) {
      Component.options.__docs = ${JSON.stringify(source)}
    }`,
    map
  )
}
