/**
 * La instancia y funciones que ayuden a trabajar con cloudinary-core
 * @file utils/cloudinary.js
 */

import cloudinary from 'cloudinary-core'

/**
 * @const cloudinaryConfiguration
 * @type {Configuration} - La configuración de cloudinary
 * @env CLOUDINARY_NAME
 */
export const cloudinaryConfiguration = new cloudinary.Configuration({
  cloud_name: process.env.CLOUDINARY_NAME || 'dinamo',
  secure: true,
})

/**
 * @const cloudinaryInstance
 * @type {Cloudinary} - La instancia de cloudinary
 */
const cloudinaryInstance = new cloudinary.Cloudinary(
  cloudinaryConfiguration.config()
)
cloudinary.Util.assign(cloudinaryInstance, cloudinary)

export default cloudinaryInstance
