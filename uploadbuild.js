/* eslint-disable no-console */
// eslint-disable-next-line
const Rsync = require('rsync')
const resolve = require('path').resolve // eslint-disable-line

const ruta = '/var/www/plantilla.dinamo.mx/source'
const usuario = 'webdev'
const servidor = 'droplet.dinamo.mx'
const buildFiles = resolve(__dirname, '.nuxt')
const staticFiles = resolve(__dirname, 'static')

const uploadBuild = new Rsync()
  .flags('cuPa')
  .progress()
  .chmod('Du=rwx,Dgo=rwx,Fu=rw,Fog=r')
  .group('www-data')
  .flags({
    'cvs-exclude': true,
    'exclude-from ./uploadignore.txt': true,
  })
  .source([buildFiles, staticFiles])
  .destination(`${usuario}@${servidor}:${ruta}`)

// simple execute
const rsyncPid = uploadBuild.execute(
  (error, code, cmd) => {
    console.log('Terminamos', cmd, code, error)
  },
  data => {
    console.log('Progresss', data.toString('utf-8'))
  },
  data => {
    console.log('Error', data)
  }
)
const quitting = () => {
  if (rsyncPid) {
    rsyncPid.kill()
  }
  process.exit()
}
process.on('SIGINT', quitting) // run signal handler on CTRL-C
process.on('SIGTERM', quitting) // run signal handler on SIGTERM
process.on('exit', quitting) // run signal handler when main process exits
