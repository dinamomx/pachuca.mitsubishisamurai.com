/**
 * Plugin para usar "mediaquieries" en js
 */

import Vue from 'vue'

const debug = (...message) =>
  // eslint-disable-next-line no-console
  console.log({
    message: [...message],
    caller: this,
  })

const sizes = {
  tablet: 769,
  desktop: 1024,
  widescreen: 1216,
  fullhd: 1408,
}
const basemedia = 'screen and'

const mediaQuerys = {
  mobile: `${basemedia} (max-width: ${sizes.tablet - 1}px)`,
  tablet: `${basemedia} (min-width: ${sizes.tablet}px)`,
  tabletOnly: `${basemedia} (min-width: ${
    sizes.tablet
  }px) and (max-width: ${sizes.desktop - 1}px)`,
  touch: `${basemedia} (max-width: ${sizes.desktop - 1}px)`,
  desktop: `${basemedia} (min-width: ${sizes.desktop}px)`,
  desktopOnly: `${basemedia} (min-width: ${
    sizes.desktop
  }px) and (max-width: ${sizes.widescreen - 1}px)`,
  widescreen: `${basemedia} (min-width: ${sizes.widescreen}px)`,
  widescreenOnly: `${basemedia} (min-width: ${
    sizes.widescreen
  }px) and (max-width: ${sizes.fullhd - 1}px)`,
  fullhd: `${basemedia} (min-width: ${sizes.fullhd}px)`,
}

if (process.client) {
  window.onNuxtReady(app => {
    debug('ready')
    app.$media.init()
  })
}
export default (ctx, inject) => {
  inject(
    'media',
    new Vue({
      name: 'MediaQuery',
      // abstract: true,
      data: () => ({
        windowWidth: 0,
        windowHeight: 0,
        mobile: false,
        tablet: false,
        tabletOnly: false,
        touch: false,
        desktop: false,
        desktopOnly: false,
        widescreen: false,
        widescreenOnly: false,
        fullhd: false,
      }),
      created() {
        debug('Created')
        this.$_matchMedia = {}
      },
      mounted() {
        this.$nextTick(() => {
          debug('Mounted')
          this.createMediaQuery()
        })
      },
      beforeDestroy() {
        Object.keys(this.$_matchMedia).forEach(key => {
          this.$_matchMedia[key].removeListener(this.setSizes.bind(this, key))
        })
      },
      methods: {
        init() {
          debug('Init')
          if (!this.$_matchMedia) {
            debug('Init matchMedia')
            this.$_matchMedia = {}
          }
          if (!this.$_matchMedia.desktop) {
            debug('Init createMediaQuery')
            this.createMediaQuery()
          } else {
            debug(this.$_matchMedia)
          }
        },
        createMediaQuery() {
          Object.keys(mediaQuerys).forEach(key => {
            this.$_matchMedia[key] = window.matchMedia(mediaQuerys[key])
            this.$_matchMedia[key].addListener(this.setSizes.bind(this, key))
            this.setSizes(key, this.$_matchMedia[key])
          })
        },
        setSizes(key, e) {
          debug(`this[${key}] = ${e.matches}`)
          debug(mediaQuerys[key])
          const w = document.documentElement.clientWidth
          const h = document.documentElement.clientHeight
          this.windowHeight = h
          this.windowWidth = w
          this[key] = e.matches
        },
      },
    })
  )
}
