/**
 * Localización para moment
 * Añadir esto a build.plugins
 * new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /es\.js/),
 *
 * Antes de usar esto considera el siguiente articulo https://github.com/you-dont-need/You-Dont-Need-Momentjs
 *
 */

// eslint-disable-next-line
import moment from 'moment'

moment.locale('es')

export default moment
