// import Vue from 'vue'

import cloudinaryInstance from '../utils/cloudinary'

const cloudinaryAttr = attr => {
  let newAttr = attr
  if (newAttr.match(/cl[A-Z]/)) newAttr = newAttr.substring(2)
  return newAttr.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase()
}

/**
 * Returns an array of attributes for cloudinary.
 * @function toCloudinaryAttributes
 * @param {Object} source - an object containing attributes
 * @param {(RegExp|string)} [filter] - copy only attributes whose name
 *  matches the filter
 * @return {Object} attributes for cloudinary functions
 */
const toCloudinaryAttributes = (source, filter) => {
  const attributes = {}
  let isNamedNodeMap
  if (window.NamedNodeMap) {
    isNamedNodeMap =
      source &&
      (source.constructor.name === 'NamedNodeMap' ||
        source instanceof NamedNodeMap)
  }
  Array.prototype.forEach.call(source, (value, name) => {
    let newName = name
    let newValue = value
    if (isNamedNodeMap) {
      /* eslint-disable prefer-destructuring */
      newName = value.name
      newValue = value.value
      /* eslint-enable prefer-destructuring */
    }
    if (!filter || filter.exec(newName)) {
      attributes[cloudinaryAttr(newName)] = newValue
    }
  })
  return attributes
}

const loadImage = (el, value, options) => {
  if (
    options.responsive === '' ||
    options.responsive === 'true' ||
    options.responsive === true
  ) {
    options.responsive = true
  }
  const url = cloudinaryInstance.url(value, options)
  if (options.responsive) {
    cloudinaryInstance.Util.setData(el, 'src', url)
    cloudinaryInstance.cloudinary_update(el, options)
    cloudinaryInstance.responsive(options, false)
  } else {
    el.setAttribute('src', url)
  }
}

const clImage = {
  inserted(el, binding) {
    const options = toCloudinaryAttributes(el.attributes)

    if (el.attributes.htmlWidth) {
      el.setAttribute('width', el.attributes.htmlWidth)
    } else {
      el.removeAttribute('width')
    }
    if (el.attributes.htmlHeight) {
      el.setAttribute('height', el.attributes.htmlHeight)
    } else {
      el.removeAttribute('height')
    }
    loadImage(el, binding.value, options)
  },

  componentUpdated(el, binding) {
    const options = toCloudinaryAttributes(el.attributes)
    loadImage(el, binding.value, options)
  },
}
export default clImage
// Vue.directive('cl-image', clImage)
