import Vue from 'vue'

const warn = msg => {
  if (!Vue.config.silent) {
    // eslint-disable-next-line no-console
    console.warn(msg)
  }
}

const isSSR = typeof window === 'undefined'
const HTMLElement = isSSR ? Object : window.HTMLElement

export default {
  name: 'IntersectObserver',
  abstract: true,
  props: {
    /**
     * IntersectionObserver threshold
     */
    threshold: {
      type: Array,
      required: false,
      default: () => [0, 0.2],
    },
    /**
     * InserectionObserver visibility ratio
     */
    ratio: {
      type: Number,
      default: 0.4,
      validator(value) {
        // can't be less or equal to 0 and greater than 1
        return value > 0 && value <= 1
      },
    },
    root: {
      type: [HTMLElement, Vue],
      required: false,
      default: () => null,
    },

    /**
     * IntersectionObserver root margin
     */
    rootMargin: {
      type: String,
      required: false,
      default: () => '0px 0px 0px 0px',
    },
  },
  mounted() {
    /* eslint-disable max-len */
    this.$nextTick(() => {
      if (this.$slots.default && this.$slots.default.length > 1) {
        warn(
          '[VueIntersect] You may only wrap one element in a <intersect> component.'
        )
      } else if (!this.$slots.default || this.$slots.default.length < 1) {
        warn(
          '[VueIntersect] You must have one child inside a <intersect> component.'
        )
        return
      }
      /* eslint-enable max-len */
      this.createObserver()
    })
  },
  methods: {
    createObserver() {
      const elementToObserve = this.root
      this.isIntersectionObserver = typeof IntersectionObserver !== 'undefined'
      if (!this.isIntersectionObserver) {
        warn(
          '[vue-intersect] IntersectionObserver API is not available in your browser. Please install this polyfill: https://github.com/WICG/IntersectionObserver/tree/gh-pages/polyfill'
        )
        this.$emit('enter')
        return
      }
      this.observer = new IntersectionObserver(
        entries => {
          if (!entries[0].isIntersecting) {
            this.$emit('leave', [entries[0]])
          } else {
            this.$emit('enter', [entries[0]])
          }
          this.$emit('change', [entries[0]])
        },
        {
          threshold: this.threshold,
          root: elementToObserve,
          rootMargin: this.rootMargin,
        }
      )
      this.observer.observe(this.$slots.default[0].elm)
    },
  },
  destroyed() {
    if (!this.isIntersectionObserver) return
    this.observer.disconnect()
    this.observer = null
  },
  render() {
    return this.$slots.default ? this.$slots.default[0] : null
  },
}
