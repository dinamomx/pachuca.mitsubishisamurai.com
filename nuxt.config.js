/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
// Cabezera
import schema from './utils/schema'

const isProd = process.env.NODE_ENV === 'production'

const meta = [
  {
    name: 'keywords',
    content: 'Diseño, Contenido, Dinamo',
  },
  {
    name: 'google-site-verification',
    content: '',
  },
]

const link = [
  {
    type: 'text/plain',
    rel: 'author',
    href: '/humans.txt',
  },
]

export default {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    meta,
    link,
    titleTemplate: (titleChunk = '') =>
      titleChunk
        ? `${titleChunk} - Mitsubishi Universidad`
        : 'Mitsubishi Universidad',
    script: [
      {
        innerHTML: JSON.stringify(schema()),
        type: 'application/ld+json',
      },
      {
        src:
          'https://polyfill.io/v2/polyfill.min.js?features=IntersectionObserver',
        body: false,
      },
    ],
    __dangerouslyDisableSanitizers: ['script'],
  },
  /**
   * Variables de entorno para la página
   */
  env: {
    productionDomain: 'mitsubishisamurai.com',
  },
  /**
   * Opciones de vue-router
   */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active--exact',
    // middleware: ['meta'],
  },
  /**
   * Transición por defecto
   */
  transition: 'page',
  /**
   * Crea un set para navegadores más vergras
   * https://nuxtjs.org/api/configuration-modern#the-modern-property
   */
  modern: isProd ? 'client' : false,
  /**
   * Enable thread-loader in webpack building
   */
  parallel: true,
  /**
   * Enable cache of terser-webpack-plugin and cache-loader
   */
  cache: true,
  /**
   * CSS global
   */
  css: [
    '@fortawesome/fontawesome-svg-core/styles.css',
    '~assets/sass/transitions.scss',
    '~assets/sass/app.scss',
    '~assets/sass/global.scss',
    // NOTE: Aquí puedes modificar el estilo de fontawesome
    // '@fortawesome/fontawesome-pro/css/fontawesome.css',
    // '@fortawesome/fontawesome-pro/css/light.css',
    // '@fortawesome/fontawesome-pro/css/regular.css',
    // '@fortawesome/fontawesome-pro/css/solid.css',
    // '@fortawesome/fontawesome-pro/css/brands.css',
  ],
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070',
  },
  /**
   * Nuxt Plugins
   */
  plugins: [
    '~/plugins/font-awesome.js',
    '~plugins/buefy.js',
    {
      src: '~plugins/scroll-track.js',
      nossr: true,
    },
  ],
  /*
   ** Build configuration
   */
  build: {
    babel: {
      presets: ({ isServer }) => [
        [
          require.resolve('@nuxt/babel-preset-app'),
          {
            buildTarget: isServer ? 'server' : 'client',
            // Incluir polyfills globales es mejor que no hacerlo
            useBuiltIns: 'entry',
            // Un poco menos de código a cambio de posibles errores
            loose: true,
            // Nuxt quiere usar ie 9, yo no.
            targets: isServer ? { node: 'current' } : {},
          },
        ],
      ],
      plugins: [
        // Reduce drásticamente el tamaño del bundle
        'lodash',
        // Si usas useBuiltIns: usage, descomenta el sig código
        // [
        //   '@babel/plugin-transform-runtime',
        //   {
        //     regenerator: true,
        //   },
        // ],
      ],
    },
    // Hace el css cacheable
    extractCSS: true,
    // Alias el ícono de buefy a uno que soporta los íconos de font-awesome
    plugins: isProd
      ? []
      : [
          new (require('webpack')).NormalModuleReplacementPlugin(
            /buefy\/src\/components\/icon\/Icon\.vue/,
            require.resolve('./components/Buefy/Icon.vue')
          ),
          new (require('lodash-webpack-plugin'))(),
        ],
    /**
     * @param {Object} config The Build Config
     * @param {Object} ctx Nuxt Context
     * @returns {Object} config
     */
    extend(config, { isDev, isClient }) {
      // Evita conflictos con el bloque de documentación
      config.module.rules.push({
        resourceQuery: /blockType=docs/,
        loader: require.resolve('./utils/docs-loader.js'),
      })
      // Arregla pnpm
      const babelLoader = config.module.rules.find(
        rule => rule.test.toString() === /\.jsx?$/.toString()
      )
      babelLoader.use[0].loader = require.resolve('babel-loader')
      // Run ESLINT on save
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules(?!\/buefy)$/,
        })
      }
      return config
    },
  },

  /**
   * Estos modulos son altamente recomendado en producción ya que facilitan
   * la incluisión de varias herramientas
   */
  modules: [
    /**
     * Limpiador de CSS
     */
    'nuxt-purgecss',
    /**
     * Faebook pixel
     */
    [
      require.resolve('./modules/facebook-pixel'),
      {
        id: '661561957280518',
        debug: process.env.NODE_ENV !== 'production',
        disable: true,
      },
    ],
    /**
     * Google Analytics, se necesita un UA válido, se puede configurar para
     * envío a diferentes cuentas o vistas de acuerdo al entorno.
     */
    [
      '@nuxtjs/google-analytics',
      {
        // Puedes usar dos analíticas
        id: process.env.PRODUCTION ? 'UA-143980-7' : 'UA-128331668-1',
        disabled: true,
        autoTracking: {
          exception: true,
          page: true,
        },
        debug: {
          enabled: process.env.NODE_ENV !== 'production',
          sendHitTask: process.env.NODE_ENV === 'production',
        },
      },
    ],
    /**
     * PWA Este módulo integra todo lo necesario para implementar las
     * capacidades PWA a una página web, require ssl
     */
    [
      '@nuxtjs/pwa',
      {
        workbox: {
          generateSW: true,
          // importScripts: ['/notifications-worker.js'],
          InjectManifest: true,
        },
      },
    ],
  ],
  /**
   * Configuración para PurgeCSS
   */
  purgeCSS: {
    whitelist: [
      'help',
      'fal',
      'has-icons-right',
      'dropdown',
      'modal',
      'dropdown-menu',
      'dropdown-content',
      'dropdown-item',
      'tag',
      'modal-background',
      'animation-content ',
      'modal-content',
    ],
    whitelistPatterns: [
      /loading-overlay/,
      /[\w|-]+-(enter|leave|move)-?(active|to)?/g,
    ],
    whitelistPatternsChildren: [/loading-overlay$/, /has-icons-.+$/, /^select/],
  },
  /**
   * Configuraciones que generan automáticamente manifest y
   * etiquetas básicas para seo
   */
  manifest: {
    name: 'Mitsubishi Universidad',
    short_name: 'Mitsubishi Universidad',
  },
  meta: {
    name: 'Mitsubishi Universidad',
    author: 'Dinamo',
    description: 'Concesionaria de autos Mitsubishi Samurai Universidad',
    theme_color: '#f83f28',
    lang: 'es',
    nativeUI: false,
    mobileApp: true,
    appleStatusBarStyle: 'default',
    ogSiteName: true,
    ogTitle: true,
    ogDescription: true,
    ogImage: true,
    ogHost: 'https://mitsubishisamurai.com',
    ogUrl: true,
    twitterCard: false,
  },
}
