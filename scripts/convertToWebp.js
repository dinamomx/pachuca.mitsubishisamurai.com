/* eslint-disable no-console */
const { CWebp } = require('cwebp')
const path = require('path')
const fs = require('fs')

const basePath = path.resolve(__dirname, '..')
const staticImages = path.resolve(basePath, 'static/images')
const assetImages = path.resolve(basePath, 'assets/images')

const filter = /(\.jpg)$|(\.jpeg)$|(\.png)$/i

const forcingOverwrite = !!process.argv[2]
function fromDir(startPath, callback) {
  if (!fs.existsSync(startPath)) {
    return
  }
  const result = {
    fileName: '',
    filePath: startPath,
    fullFileName: '',
  }

  const files = fs.readdirSync(startPath)
  for (let i = 0; i < files.length; i++) {
    const fileName = files[i]
    const fullFileName = path.join(startPath, fileName)
    const stat = fs.lstatSync(fullFileName)
    result.fileName = fileName
    result.fullFileName = fullFileName
    if (stat.isDirectory()) {
      fromDir(fullFileName, callback) // recurse
    } else if (filter.test(fileName)) {
      callback(result)
    }
  }
}

const convertImageCallback = error => {
  console.log(error || 'Archivo convertido con éxito')
}

function convertImage({ fileName, filePath, fullFileName }) {
  const newName = fileName.replace(filter, '.webp')
  const newFilePath = `${filePath}/${newName}`
  if (fs.existsSync(newFilePath)) {
    console.warn(`Ya existe el archivo ${newFilePath}`)
    if (!forcingOverwrite) {
      console.warn(
        'Abortando, usa el argumento force para sobreescribir archivos'
      )
      return
    }
  }
  console.log(`Converting ${fileName} to ${newName}`)
  const encoder = new CWebp(fullFileName)
  encoder.quality(100)
  encoder.nearLossless(100)
  encoder.write(newFilePath, convertImageCallback)
}

fromDir(assetImages, convertImage)
fromDir(staticImages, convertImage)
