const { resolve } = require('path')

module.exports = function module(moduleOptions) {
  const options = this.options['facebook-pixel'] || moduleOptions

  this.addPlugin({
    src: resolve(__dirname, './templates/plugin.template.js'),
    fileName: 'facebook-pixel.js',
    options,
    ssr: false,
  })
}
