/* eslint-disable */
import Vue from 'vue'
import VueFacebookPixel from '~/modules/facebook-pixel/plugins/vue-facebook-pixel'

export default async function ({ app: { router } }) {
  const moduleOptions = <%= serialize(options) %>
  Vue.use(VueFacebookPixel, Object.assign({ router }, moduleOptions))
}
