/* TEAM */
	Desarrollador y diseñador UI/UX: Ramses Reyes
	Twitter: @_ramjam
	From: Ciudad de México, México

	Desarrollador Frontend: César Valadez
	Twitter: @cesasol
	From: Ciudad de México, México

	Desarrolladora Frontend: Montserrat Soto
	From: Ciudad de México, México

  Diseño Estratégico: Seth Gonzalez
	From: Ciudad de México, México

	Director de Arte: Jiovany Cruz
	From: Ciudad de México, México

	Gerente de Ventas y Atención al Cliente: Tanya González
	From: Ciudad de México, México

	Social Media Management: Diego Villamar
	From: Ciudad de México, México

	Creativo: Erika Pureco
	From: Ciudad de México, México

	Diseñador Gráfico: Omar Rojas
	From: Ciudad de México, México

	Social Media Management: Daniela Quiñonez
	From: Ciudad de México, México

/* THANKS */

	All of the open source developers that made this development possible

/* SITE */
	Last update: 2018/08/30
	Language: Español
	Doctype: HTML5
	IDE: Visual Studio Code, Figma
