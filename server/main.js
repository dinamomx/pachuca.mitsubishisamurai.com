/* eslint-disable no-console */
import Koa from 'koa'
import { Nuxt, Builder } from 'nuxt'
import http from 'http'
import gracefulShutdown from 'http-graceful-shutdown'
import config from '../nuxt.config'

require('dotenv').config()

const isDev = process.env.NODE_ENV !== 'production'
const app = new Koa()
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000
process.send = process.send || function fakeSend() {}

// Import and Set Nuxt.js options
config.dev = isDev

async function start() {
  // Instantiate nuxt.js
  const nuxt = new Nuxt(config)

  // Build in development
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  app.use(async (ctx, next) => {
    await next()
    ctx.status = 200 // koa defaults to 404 when it sees that status is unset
    return new Promise((resolve, reject) => {
      ctx.res.on('close', resolve)
      ctx.res.on('finish', resolve)
      nuxt.render(ctx.req, ctx.res, promise => {
        // nuxt.render passes a rejected promise into callback on error.
        promise.then(resolve).catch(reject)
      })
    })
  })

  const server = http.createServer(app.callback())

  gracefulShutdown(server, {
    development: isDev,
    finally: () => {
      console.log('Server gracefulls shutted down.....')
    },
  })

  server.listen(port, host, () => {
    console.log('Server listening on:')
    console.log(`  http://${host}:${port.toString()}`)
    process.send('ready')
  })
}

start()
